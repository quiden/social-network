import profileReducer from "./profileReducer";
import dialogsReducer from "./dialogsReducer";
import sidebarReducer from "./sidebarReducer";

let store = {
    _state: {
        profilePage: {
            postsData: [
                {
                    id: 1,
                    message: "Hi, how are you?",
                    likesCount: 21
                },
                {
                    id: 2,
                    message: `It's my first post!`,
                    likesCount: 10
                },
                {
                    id: 3,
                    message: `Hello!`,
                    likesCount: 0
                },
                {
                    id: 4,
                    message: `Ahhahaha!`,
                    likesCount: 70
                },
                {
                    id: 6,
                    message: `Hello World!`,
                    likesCount: 100
                }
            ],
            newPostText: ""
        },
        messagesPage: {
            dialogsData: [
                {
                    id: 1,
                    name: "Mikhail",
                    avatar: "https://sm.ign.com/ign_ap/cover/a/avatar-gen/avatar-generations_hugw.jpg"
                },
                {
                    id: 2,
                    name: "Kirill",
                    avatar: "https://png.pngtree.com/png-vector/20190710/ourmid/pngtree-user-vector-avatar-png-image_1541962.jpg"
                },
                {
                    id: 3,
                    name: "Pavel",
                    avatar: "https://e7.pngegg.com/pngimages/799/987/png-clipart-computer-icons-avatar-icon-design-avatar-heroes-computer-wallpaper.png"
                },
                {
                    id: 4,
                    name: "Vladimir",
                    avatar: "https://banner2.cleanpng.com/20180303/esq/kisspng-web-development-organization-world-wide-web-icon-men-s-avatar-5a9b58f5b7c517.0692693715201302937527.jpg"
                },
                {
                    id: 5,
                    name: "Konstantin",
                    avatar: "https://w7.pngwing.com/pngs/340/946/png-transparent-avatar-user-computer-icons-software-developer-avatar-child-face-heroes-thumbnail.png"
                },
            ],
            messagesData: [
                {
                    id: 1,
                    message: "Hello. How are you?",
                    myMessage: false
                },
                {
                    id: 2,
                    message: "I am fine!",
                    myMessage: true
                },
                {
                    id: 3,
                    message: "Let's go",
                    myMessage: false
                },
                {
                    id: 4,
                    message: "Yo",
                    myMessage: false
                },
                {
                    id: 5,
                    message: "Ahahahah",
                    myMessage: true
                },
            ],
            newMessageText: ""
        },
        sidebar: {
            friends: [
                {
                    avatar: "https://png.pngtree.com/png-vector/20190223/ourmid/pngtree-vector-avatar-icon-png-image_695765.jpg",
                    name: "Aleksandr"
                },
                {
                    avatar: "https://png.pngtree.com/png-vector/20190223/ourmid/pngtree-vector-avatar-icon-png-image_695765.jpg",
                    name: "Kirill"
                },
                {
                    avatar: "https://png.pngtree.com/png-vector/20190223/ourmid/pngtree-vector-avatar-icon-png-image_695765.jpg",
                    name: "Andrew"
                },
            ]
        }

    },
    _callSubscriber() {
        console.log("no subscribers (observers)");
    },
    getProfilePage() {
        return this._state.profilePage;
    },
    getMessagesPage() {
        return this._state.messagesPage;
    },
    getSidebar() {
        return this._state.sidebar;
    },
    subscribe(observe) {
        this._callSubscriber = observe;
    },
    dispatch(action) {
        this._state.profilePage = profileReducer(this._state.profilePage, action);
        this._state.messagesPage = dialogsReducer(this._state.messagesPage, action);
        this._state.sidebar = sidebarReducer(this._state.sidebar, action);
        this._callSubscriber();
    }
}

export default store;
window.store = store;
