const SEND_MESSAGE = "SEND-MESSAGE";
const UPDATE_NEW_MESSAGE_TEXT = "UPDATE-NEW-MESSAGE-TEXT";

const initialState = {
    dialogsData: [
        {
            id: 1,
            name: "Mikhail",
            avatar: "https://sm.ign.com/ign_ap/cover/a/avatar-gen/avatar-generations_hugw.jpg"
        },
        {
            id: 2,
            name: "Kirill",
            avatar: "https://png.pngtree.com/png-vector/20190710/ourmid/pngtree-user-vector-avatar-png-image_1541962.jpg"
        },
        {
            id: 3,
            name: "Pavel",
            avatar: "https://e7.pngegg.com/pngimages/799/987/png-clipart-computer-icons-avatar-icon-design-avatar-heroes-computer-wallpaper.png"
        },
        {
            id: 4,
            name: "Vladimir",
            avatar: "https://banner2.cleanpng.com/20180303/esq/kisspng-web-development-organization-world-wide-web-icon-men-s-avatar-5a9b58f5b7c517.0692693715201302937527.jpg"
        },
        {
            id: 5,
            name: "Konstantin",
            avatar: "https://w7.pngwing.com/pngs/340/946/png-transparent-avatar-user-computer-icons-software-developer-avatar-child-face-heroes-thumbnail.png"
        },
    ],
    messagesData: [
        {
            id: 1,
            message: "Hello. How are you?",
            myMessage: false
        },
        {
            id: 2,
            message: "I am fine!",
            myMessage: true
        },
        {
            id: 3,
            message: "Let's go",
            myMessage: false
        },
        {
            id: 4,
            message: "Yo",
            myMessage: false
        },
        {
            id: 5,
            message: "Ahahahah",
            myMessage: true
        },
    ],
        newMessageText: ""
};

const dialogsReducer = (state = initialState, action) => {
    switch (action.type) {
        case SEND_MESSAGE: {
            const newMessage = {
                id: 6,
                message: state.newMessageText,
                myMessage: true
            };
            return {
                ...state,
                newMessageText: "",
                messagesData: [...state.messagesData, newMessage],
            };
        }
        case UPDATE_NEW_MESSAGE_TEXT: {
            return {
                ...state,
                newMessageText: action.newMessage
            };
        }
        default: {
            return state;
        }
    }
}

export const sendMessageCreator = () => ({type: SEND_MESSAGE});
export const updateNewMessageTextCreator = (text) => {
    return {
        type: UPDATE_NEW_MESSAGE_TEXT,
        newMessage: text
    }
}

export default dialogsReducer;
