import {authAPI} from "../api/authApi";
import {profileAPI} from "../api/profileApi";

const SET_USER_DATA = "SET_USER_DATA";
const SET_USER_PROFILE_DATA = "SET_USER_PROFILE_DATA";

const initialState = {
    userId: null,
    email: null,
    login: null,
    isAuth: false,
    profileData: null
}

const authReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_USER_DATA:
            return {
                ...state,
                ...action.data,
                isAuth: true
            }
        case SET_USER_PROFILE_DATA:
            return {
                ...state,
                profileData: {...action.profileData}
            }
        default:
            return state;
    }
}

const setAuthUserData = (userId, email, login) => ({
    type: SET_USER_DATA,
    data: {userId, email, login}
});

export const getAuthUserData = () => (dispatch) => {
    authAPI.getAuthMe().then((data) => {
        if (data.resultCode === 0) {
            let {id, email, login} = data.data;
            dispatch(setAuthUserData(id, email, login));
            profileAPI.getUserProfile(data.data.id).then((data) => {
                dispatch(setProfileUserData(data));
            })
        }
    })
}

export const authorization = (formData) => (dispatch) => {
    authAPI.authorization(formData)
        .then(response => {
            if (response.resultCode === 0) {
                dispatch(getAuthUserData());
            } else {
                console.log("no auth!!!");
            }
        })
}

export const setProfileUserData = (profileData) => ({
    type: SET_USER_PROFILE_DATA,
    profileData
});

export default authReducer;