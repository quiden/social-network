import {profileAPI} from "../api/profileApi";

const ADD_POST = "ADD-POST";
const UPDATE_NEW_POST_TEXT = "UPDATE-NEW-POST-TEXT";
const SET_USER_PROFILE = "SET_USER_PROFILE";
const SET_STATUS = "SET_STATUS";

const initialState = {
    postsData: [
        {
            id: 1,
            message: "Hi, how are you?",
            likesCount: 21
        },
        {
            id: 2,
            message: `It's my first post!`,
            likesCount: 10
        },
        {
            id: 3,
            message: `Hello!`,
            likesCount: 0
        },
        {
            id: 4,
            message: `Ahhahaha!`,
            likesCount: 70
        },
        {
            id: 6,
            message: `Hello World!`,
            likesCount: 100
        }
    ],
    newPostText: "",
    profile: null,
    status: ""
};


const profileReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_POST: {
            const newPost = {
                id: 5,
                message: state.newPostText,
                likesCount: 0
            };
            return {
                ...state,
                postsData: [...state.postsData, newPost],
                newPostText: ""
            };
        }
        case UPDATE_NEW_POST_TEXT: {
            return {
                ...state,
                newPostText: action.newText,
            };
        }
        case SET_USER_PROFILE: {
            return {
                ...state,
                profile: action.profile
            }
        }
        case SET_STATUS: {
            return {
                ...state,
                status: action.status
            }
        }
        default: {
            return state;
        }
    }
}

export const addPostActionCreator = () => ({type: ADD_POST});
export const updateNewPostTextActionCreator = (text) => {
    return {
        type: UPDATE_NEW_POST_TEXT,
        newText: text
    }
};

const setUserProfile = (profile) => ({
    type: SET_USER_PROFILE,
    profile
});

const setStatus = (status) => ({
    type: SET_STATUS,
    status
});

export const getUserProfile = (userId) => (dispatch) => (
    profileAPI.getUserProfile(userId)
        .then(data => {
            dispatch(setUserProfile(data));
        })
);

export const getUserStatus = (userId) => (dispatch) => {
    profileAPI.getUserStatus(userId)
        .then(data => {
            dispatch(setStatus(data))
        })
}

export const updateUserStatus = (status) => (dispatch) => {
    debugger
    profileAPI.updateUserStatus(status)
        .then((response) => {
            if (response.data.resultCode === 0) {
                dispatch(setStatus(status))
            }
        })
}

export default profileReducer;
