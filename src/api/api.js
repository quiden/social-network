import axios from "axios";

export const instance = axios.create({
    withCredentials: true,
    baseURL: "https://social-network.samuraijs.com/api/1.0/",
    headers: {
        "API-KEY": "30d8203a-ae97-46d7-a38f-cbc620e4fa40"
    }
});