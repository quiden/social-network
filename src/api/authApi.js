import { instance } from "./api";

export const authAPI = {
    getAuthMe() {
        return instance.get("auth/me")
            .then(response => response.data);
    },

    authorization(formData) {
        return instance.post("auth/login", formData);
    }
}