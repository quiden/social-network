import React from "react";
import { Field, reduxForm } from "redux-form";

const LoginForm = (props) => {
    return (
        <form onSubmit={props.handleSubmit}>
            <div>
                <Field component={"input"} name={"login"} type="text" placeholder={"login"}/>
            </div>
            <div>
                <Field component={"input"} name={"password"} type="password" placeholder={"password"}/>
            </div>
            <div>
                <Field component={"input"} name={"rememberMe"} type="checkbox"/> remember me
            </div>
            <div>
                <button>Login</button>
            </div>
        </form>
    );
}

const LoginReduxForm = reduxForm({
    form: "login"
})(LoginForm);

const Login = (props) => {
    return <div>
        <h1>Login</h1>
        <LoginReduxForm onSubmit={props.authorization}/>
    </div>
};

export default Login;