import React from "react";
import {connect} from "react-redux";
import {authorization} from "../../redux/authReducer";
import Login from "./Login";

const mapStateToProps = (state) => ({

});

const LoginContainer = connect(mapStateToProps, {
    authorization
})(Login);

export default LoginContainer;
