import React from "react";
import styles from "./Navbar.module.css";
import { NavLink } from "react-router-dom";

const Navbar = (props) => {
    return (
        <nav className={styles.nav}>
            <div className={styles.item}>
                <NavLink to="/profile" className={(navData) => navData.isActive ? styles.activeLink: undefined}>Profile</NavLink>
            </div>
            <div className={styles.item}>
                <NavLink to="/dialogs" className={(navData) => navData.isActive ? styles.activeLink: undefined}>Messages</NavLink>
            </div>
            <div className={styles.item}>
                <NavLink to="/users" className={(navData) => navData.isActive ? styles.activeLink: undefined}>Users</NavLink>
            </div>
            <div className={styles.item}>
                <NavLink to="/news" className={(navData) => navData.isActive ? styles.activeLink: undefined}>News</NavLink>
            </div>
            <div className={styles.item}>
                <NavLink to="/music" className={(navData) => navData.isActive ? styles.activeLink: undefined}>Music</NavLink>
            </div>
            <div className={styles.item}>
                <NavLink to="/settings" className={(navData) => navData.isActive ? styles.activeLink: undefined}>Settings</NavLink>
            </div>
            <div className={styles.friends}>
                <h2>Friends</h2>
                <div className={styles.myFriends}>
                    {props.sidebar.friends.map((friend) => {
                        return (
                            <div className={styles.friend} key={friend.id}>
                                <img src={friend.avatar} width={40} height={40} alt="avatar" />
                                {friend.name}
                            </div>
                        );
                    })}
                </div>
            </div>
        </nav>
    );
}

export default Navbar;