import React from "react";
import styles from "./ProfileInfo.module.css";
import MyPostsContainer from "../MyPost/MyPostsContainer";
import ProfileStatus from "./ProfileStatus";

const ProfileInfo = (props) => {
    return <>
        <div className={styles.profileHeader}>
            <div className={styles.pageCover}>
                <img
                    src="https://marketplace.canva.com/EAFH2x7hYI4/1/0/1600w/canva-beige-minimalist-shape-twitter-header-VVKGEYQ89Ho.jpg"
                    height={170} alt={"cover"}/>
                <button>Edit Cover Photo</button>
            </div>
            <div className={styles.descriptionBlock}>
                <img className={styles.avatar} src={props.profile?.photos?.large} width={100} height={100} alt=""/>
                <h3>{props.profile?.fullName}</h3>
                <ProfileStatus status={props.status} updateUserStatus={props.updateUserStatus}/>
                <p>{props.profile?.aboutMe}</p>
                <button>Edit basic info</button>
            </div>
        </div>
        <div className={styles.splitLayout}>
            <div>
                <h4>Intro</h4>
                <ul>
                    <li><a href={props.profile?.contacts.facebook}>facebook</a></li>
                    <li>website</li>
                    <li>vk</li>
                    <li>twitter</li>
                    <li>instagram</li>
                    <li>youtube</li>
                    <li>github</li>
                    <li>mainLink</li>
                </ul>
            </div>
            <MyPostsContainer />
            <div>
                <h4>About job</h4>
                { props.profile?.lookingForAJob ? <div>
                    <p>Looking for a job</p>
                    <h5>Description</h5>
                    <p>{props.profile?.lookingForAJobDescription}</p>
                </div>: <p>No</p>
                }
            </div>
        </div>
    </>
}

export default ProfileInfo;