import React from "react";
import styles from './Profile.module.css';
import ProfileInfo from "./ProfileInfo/ProfileInfo";
import {updateUserStatus} from "../../redux/profileReducer";

const Profile = (props) => {
    return (
        <div className={styles.profile}>
            <ProfileInfo profile={props.profile} status={props.status} updateUserStatus={props.updateUserStatus}/>
        </div>
    );
}

export default Profile;