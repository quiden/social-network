import React from "react";
import classes from "./post.module.css";

const Post = (props) => {
    return (
        <div className={classes.item}>
            <img src="https://png.pngtree.com/png-vector/20220709/ourmid/pngtree-businessman-user-avatar-wearing-suit-with-red-tie-png-image_5809521.png" height={50} alt="avatar" />
            {props.message}
            <div>
                <span>{props.likeCounts} like</span>
            </div>
        </div>
    );
}

export default Post;