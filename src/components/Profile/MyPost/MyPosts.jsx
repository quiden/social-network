import React from 'react';
import styles from './myPosts.module.css';
import Post from "./Post/Post";

const MyPosts = (props) => {
    const newPostElement = React.createRef();

    const onPostAdd = () => {
        props.addPost();
    }

    const onPostChange = () => {
        const text = newPostElement.current.value;
        props.updateNewPostText(text);
    }

    return (
        <div className={styles.postsBlock}>
            <h3>My posts</h3>
            <div>
                New post
                <div>
                    <textarea ref={newPostElement} onChange={onPostChange} value={props.newPostText} />
                </div>
                <div>
                    <button onClick={ onPostAdd }>Add post</button>
                    <button>Remove</button>
                </div>
            </div>
            <div className={styles.posts}>
                {
                    props.postsData?.map(post => <Post message={post.message} likeCounts={post.likesCount}/>)
                }
            </div>
        </div>
    );
}

export default MyPosts;
