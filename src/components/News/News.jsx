import React from "react";
import styles from './News.module.css';

const News = (props) => {
    return (
        <div className={styles.container}>
            <div className={`${styles.item} ${styles.item1}`}>1</div>
            <div className={`${styles.item} ${styles.item3}`}>2</div>
            <div className={`${styles.item} ${styles.item3}`}>3</div>
            <div className={`${styles.item} ${styles.item4}`}>4</div>
            <div className={`${styles.item} ${styles.item5}`}>5</div>
            <div className={`${styles.item} ${styles.item6}`}>6</div>
            <div className={`${styles.item} ${styles.item7}`}>7</div>
            <div className={`${styles.item} ${styles.item8}`}>8</div>
            <div className={`${styles.item} ${styles.item9}`}>9</div>
        </div>
    );
}

export default News;