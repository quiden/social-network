import React from "react";
import userPhoto from "../../assets/images/userPhoto.png";
import axios from "axios";
import styles from "./Users.module.css";
import {NavLink} from "react-router-dom";
import {UsersAPI} from "../../api/usersApi";

const Users = (props) => {

    const pagesCount = Math.ceil(props.totalUsersCount / props.pageSize);

    const pages = [];
    for (let i = 1; i <= pagesCount; i++) {
        pages.push(i);
    }

    return (
        <div>
            {pages?.map((page) => <span className={props.currentPage === page && styles.selectedPage}
                                        onClick={(e) => props.onPageChanged(page)}>{page}</span>)}
            {props.users?.map((user) => {
                return <div key={user.id}>
                    <div>
                        <div>
                            <NavLink to={'/profile/' + user.id}>
                                <img src={user.photos.small != null ? user.photos.small : userPhoto} width={60}
                                     height={60}
                                     alt="avatar"/>
                            </NavLink>
                        </div>
                        <div>
                            {!user.followed ? <button disabled={props.followingInProgress.some(id => id === user.id)} onClick={() => {
                                    this.props.follow(user.id);
                                }
                                }>Follow</button> :
                                <button disabled={props.followingInProgress.some(id => id === user.id)} onClick={() => {
                                    this.props.unfollow(user.id);
                                }}>Unfollow</button>}
                        </div>
                    </div>
                    <div>
                        <div>
                            <div>{user.name}</div>
                            <div>{user.status}</div>
                        </div>
                        <div>
                            <div>{"user.country"}</div>
                            <div>{"user.city"}</div>
                        </div>
                    </div>
                </div>
            })}
        </div>
    )
}

export default Users;