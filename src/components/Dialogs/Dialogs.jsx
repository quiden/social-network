import React from 'react';
import styles from './Dialogs.module.css';
import DialogItem from "./DialogItem/DialogItem";
import Message from "./Message/Message";

const Dialogs = (props) => {
    const dialogsElements = props.dialogsData
        .map(dialog => <DialogItem name={dialog.name} key={dialog.id} id={dialog.id} avatar={dialog.avatar} />);

    const messagesElements = props.messagesData
        .map(message => <Message message={message.message} key={message.id} myMessage={message.myMessage} />);

    const newMessageElement = React.createRef();

    const onSendMessageClick = () => {
        props.sendMessage();
    }

    const onMessageTextChange = (e) => {
        const text = e.target.value;
        props.updateNewMessageText(text);
    }
    return (
        <div className={styles.dialogs}>
            <div className={styles.dialogsItems}>
                {
                    dialogsElements
                    // dialogsData.map((dialog) => {
                    //     return <DialogItem name={dialog.name} id={dialog.id} />;
                    // })
                }
            </div>
            <div className={styles.messages}>
                <div>{messagesElements}</div>
                <div>
                    <textarea ref={newMessageElement} onChange={(e) => onMessageTextChange(e)}
                             value={props.newMessageText} placeholder="Enter your message"></textarea>
                    <button onClick={onSendMessageClick}>Send</button>
                </div>
            </div>
        </div>
    );
}

export default Dialogs;