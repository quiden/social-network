import React from 'react';
import styles from './../Dialogs.module.css';

const Message = (props) => {
    return (props.myMessage ?
            <div className={`${styles.message} ${styles.myMessage}`}>
                 {props.message}
                <img src="https://ionicframework.com/docs/img/demos/avatar.svg" width={30} height={30} alt="avatar"/>
            </div> :
            <div className={styles.message}>
                <img src="https://ionicframework.com/docs/img/demos/avatar.svg" width={30} height={30} alt="avatar"/>
                {props.message}
            </div>
    );
}

export default Message;