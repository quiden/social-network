import React from 'react';
import { NavLink } from "react-router-dom";
import styles from './../Dialogs.module.css';

const DialogItem = (props) => {
    let path = `/dialogs/${props.id}`;

    return (
        <div className={styles.dialog}>
            <img src={props.avatar} width={30} height={30} alt="avatar" />
            <NavLink to={path}>{props.name}</NavLink>
        </div>
    );
}

export default DialogItem;