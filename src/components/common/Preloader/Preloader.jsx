import React from 'react';
import { Spin } from 'antd';

const Preloader = (props) => {
    return <Spin size="large"/>
}

export default Preloader;