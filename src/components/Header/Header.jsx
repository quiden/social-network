import React from 'react';
import styles from './Header.module.css';
import {NavLink} from "react-router-dom";

const Header = (props) => {
    return (
        <header className={styles.header}>
            <div className={styles.wrap}>
                <div className={styles.logo}>
                    <img
                        src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/24/LEGO_logo.svg/2048px-LEGO_logo.svg.png"
                        width={30}
                        height={30}
                        alt="log"/>
                    <h1>Social Network</h1>
                </div>
                <div className={styles.search}>
                    <input></input>
                </div>
                <div className={styles.notify}>
                    <img alt="notifications"/>
                </div>
                <div className={styles.profile}>
                    {props.isAuth ? <div>
                            <p>{props.login}</p>
                            <img src={props.profilePage?.photos.small} alt="portrait"/>
                        </div>
                        : <NavLink to={"/login"}>login</NavLink>}
                </div>
            </div>
        </header>
    );
}

export default Header;