import React from 'react';
import { Route, Routes } from "react-router-dom";
import './App.css';
import News from "./components/News/News";
import Settings from "./components/Settings/Settings";
import Music from "./components/Music/Music";
import DialogsContainer from "./components/Dialogs/DialogsContainer";
import NavbarContainer from "./components/Navbar/NavbarColtainer";
import UsersContainer from "./components/Users/UsersContainer";
import ProfileContainer from "./components/Profile/ProfileContainer";
import HeaderContainer from "./components/Header/HeaderContainer";
import Login from "./components/Login/Login";
import LoginContainer from "./components/Login/LoginContainer";

const App = (props) => {
    return (
        <div className="app-wrapper">
            <HeaderContainer />
            {/*<div className="app-wrapper-layout">*/}
            <NavbarContainer />
            <div className="app-wrapper-content">
                <Routes>
                    <Route path="/dialogs" element={<DialogsContainer />} />
                    <Route path="/profile/:userId?" element={<ProfileContainer  />} />
                    <Route path="/news" element={<News />}/>
                    <Route path="/music" element={<Music />}/>
                    <Route path="/users" element={<UsersContainer />}/>
                    <Route path="/settings" element={<Settings />}/>
                    <Route path="/login" element={<LoginContainer />}/>
                </Routes>
            </div>
            {/*</div>*/}
        </div>
    );
}

export default App;
